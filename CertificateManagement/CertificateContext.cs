﻿using System.Data.Entity;

namespace CertificateManagement
{
    public class CertificateContext : DbContext
    {
        public CertificateContext()
            : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\CertificateManagement.CertificateContext.mdf;Integrated Security=True;Connect Timeout=30")
        {
            Database.Initialize(true);
            Database.SetInitializer(new DropCreateDatabaseAlways<CertificateContext>());
        }

        public DbSet<Cetificate> Cetificates { get; set; }
    }
}