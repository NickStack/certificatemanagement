﻿namespace CertificateManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renametable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PFXCetificates", newName: "Cetificates");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Cetificates", newName: "PFXCetificates");
        }
    }
}
