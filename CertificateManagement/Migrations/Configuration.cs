﻿using BL.Auth.Server.Certificate;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using WebApplication;

namespace CertificateManagement.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CertificateContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CertificateContext context)
        {
            var gateWayCertificate = BLCertificate.GetGateWayCertificate();
            var driverSIPolicyCertificate = BLCertificate.GetDriverSIPolicyCertificate();
            var authServerCertificate = BLCertificate.GetAuthServerCertificate();

            var settings = new JsonSerializerSettings
            {
                Converters = new List<JsonConverter>() { new CustomX509Certificate2Converter() }
            };

            var gateWayCertificateSerialized = JsonConvert.SerializeObject((object)gateWayCertificate, settings);
            var driverSIPolicyCertificateSerialized = JsonConvert.SerializeObject((object)driverSIPolicyCertificate, settings);
            var authServerCertificateSerialized = JsonConvert.SerializeObject((object)authServerCertificate, settings);

            context.Cetificates.Add(new Cetificate() { Certificate = gateWayCertificateSerialized });
            context.Cetificates.Add(new Cetificate() { Certificate = driverSIPolicyCertificateSerialized });
            context.Cetificates.Add(new Cetificate() { Certificate = authServerCertificateSerialized });

            context.SaveChanges();
        }
    }
}
