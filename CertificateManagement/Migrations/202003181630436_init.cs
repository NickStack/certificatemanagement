﻿using System.Data.Entity.Migrations;

namespace CertificateManagement.Migrations
{
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PFXCetificates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Certificate = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PFXCetificates");
        }
    }
}