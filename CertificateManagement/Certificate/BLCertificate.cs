﻿using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace BL.Auth.Server.Certificate
{
    internal static class BLCertificate
    {
        private const string BaseCertificatePath = "CertificateManagement.Certificate.";
        private const string AuthServerCertificatePassword = "w*fW^x51zJBL";

        private static string AuthServerCertificatePath => $"{BaseCertificatePath}BLAuthServer.pfx";
        private static string GatewayCertificatePath => $"{BaseCertificatePath}gateway.cer";
        private static string DriverSIPolicyCertificatePath => $"{BaseCertificatePath}driversipolicy.p7b";


        public static X509Certificate2 GetAuthServerCertificate()
        {
            var certificate = GetCertificate(AuthServerCertificatePath, AuthServerCertificatePassword);

            return certificate;
        }

        public static X509Certificate2 GetGateWayCertificate()
        {
            var certificate =  GetCertificate(GatewayCertificatePath);

            return certificate;
        }

        public static X509Certificate2 GetDriverSIPolicyCertificate()
        {
            var certificate = GetCertificate(DriverSIPolicyCertificatePath);

            return certificate;
        }

        public static X509Certificate2 GetMarriottSamlCertificate(string fileNamePostfix)
        {
            var certificateInfo = MarriottGeneralCertificateInfoProvider.Provider[fileNamePostfix];

            var certificate = GetCertificate(certificateInfo.CertificatePath, certificateInfo.Password);

            return certificate;
        }

        private static X509Certificate2 GetCertificate(string path)
        {
            var assembly = typeof(BLCertificate).Assembly;
            using (var stream = assembly.GetManifestResourceStream(path))
            {
                return new X509Certificate2(ReadStream(stream));
            }
        }

        private static X509Certificate2 GetCertificate(string path, string password)
        {
            var assembly = typeof(BLCertificate).Assembly;
            using (var stream = assembly.GetManifestResourceStream(path))
            {
                return new X509Certificate2(ReadStream(stream), password, X509KeyStorageFlags.Exportable);
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            using (var memoryStream = new MemoryStream())
            {
                input.CopyTo(memoryStream);

                return memoryStream.ToArray();
            }
        }



        private static class MarriottGeneralCertificateInfoProvider
        {
            private static string MarriottSamlCertificatePath => $"{ BaseCertificatePath}BLMarriottSaml{{0}}.pfx";


            public static readonly IReadOnlyDictionary<string, GeneralCertificateInfo> Provider;


            static MarriottGeneralCertificateInfoProvider()
            {
                Provider = new Dictionary<string, GeneralCertificateInfo>()
                {
                    {
                        "dev",
                        new GeneralCertificateInfo()
                        {
                            CertificatePath = string.Format(MarriottSamlCertificatePath, "-dev"),
                            Password = "n~CbkEX6T[P84/2;"
                        }
                    },
                    {
                        "live",
                        new GeneralCertificateInfo()
                        {
                            CertificatePath = string.Format(MarriottSamlCertificatePath, "-live"),
                            Password = "QR5N6sg4u547"
                        }
                    }
                };
            }
        }



        private class GeneralCertificateInfo
        {
            public string Password { get; set; }

            public string CertificatePath { get; set; }
        }
    }
}