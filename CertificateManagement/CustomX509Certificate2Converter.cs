﻿using Newtonsoft.Json;
using System;
using System.Security.Cryptography.X509Certificates;

namespace WebApplication
{
    public class CustomX509Certificate2Converter : JsonConverter
    {
        public CustomX509Certificate2Converter()
        {

        }
        public override bool CanConvert(Type objectType)
        {
            return typeof(X509Certificate2) == objectType;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is X509Certificate2 x509Certificate2))
                return;
            serializer.Serialize(writer, (object)Convert.ToBase64String(x509Certificate2.Export(x509Certificate2.HasPrivateKey ? X509ContentType.Pfx : X509ContentType.Cert)));
        }

        public override object ReadJson(
          JsonReader reader,
          Type objectType,
          object existingValue,
          JsonSerializer serializer)
        {
            return (object)new X509Certificate2(Convert.FromBase64String(serializer.Deserialize<string>(reader)));
        }
    }
}