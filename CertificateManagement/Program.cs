﻿using BL.Auth.Server.Certificate;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using WebApplication;

namespace CertificateManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            var gateWayCertificate = BLCertificate.GetGateWayCertificate();
            var driverSIPolicyCertificate = BLCertificate.GetDriverSIPolicyCertificate();
            var authServerCertificate = BLCertificate.GetAuthServerCertificate();

            var settings = new JsonSerializerSettings();
            settings.Converters = new List<JsonConverter>() { new CustomX509Certificate2Converter() };

            using (var ctx = new CertificateContext())
            {
                var certificates = ctx.Cetificates.ToList();
                var gateWayCertificateDB = certificates.First();
                var gateWayCertificateDeserialized = JsonConvert.DeserializeObject<X509Certificate2>(gateWayCertificateDB.Certificate, settings);

                Console.WriteLine(gateWayCertificate.Equals(gateWayCertificateDeserialized));

                var driverSIPolicyCertificateDB = certificates.Skip(1).First();
                var driverSIPolicyCertificateDeserialized = JsonConvert.DeserializeObject<X509Certificate2>(driverSIPolicyCertificateDB.Certificate, settings);

                Console.WriteLine(driverSIPolicyCertificate.Equals(driverSIPolicyCertificateDeserialized));

                var authServerCertificateDB = certificates.Last();
                var authServerCertificateDeserialized = JsonConvert.DeserializeObject<X509Certificate2>(authServerCertificateDB.Certificate, settings);

                Console.WriteLine(authServerCertificate.Equals(authServerCertificateDeserialized));
                ctx.SaveChanges();
            }
        }
    }
}